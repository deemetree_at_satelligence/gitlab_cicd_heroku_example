# gitlab_cicd_heroku_example

Example of a gitlab based CI/CD project to deploy to Heroku

## Presentation slides:

1. HLW: How to Git (05-02-2020): [Google Slides](https://docs.google.com/presentation/d/1IbAkd4Yu5g9G07Ny66PMNcBC2IkJiQPAvPZjIuC9Msk/edit?usp=sharing)  
1. HLW: DevOps (06-02-2020): [Google Slides](https://docs.google.com/presentation/d/10al6NWuMsi2LdcL4pgJG9miHYvkF3llJzx1GfrqnrHg/edit?usp=sharing)  

## What do you need

1. [NPM and Node.js installed](https://nodejs.org/en/download/)

## Run locally

The application is run on port 5000 locally.  
There are two routes in the application: `localhost:5000/` and `localhost:5000/cats`.  

1. `$ npm start`

## What to do

1. Fork this project to your own profile
2. Try it out on
3. Adjust .gitlab-ci.yml
4. Push your code